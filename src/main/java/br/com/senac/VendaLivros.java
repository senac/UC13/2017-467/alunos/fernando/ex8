/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author sala304b
 */
public class VendaLivros {
    private String cliente;
    private double valor;
    private String estado;

    public VendaLivros(String cliente, double valor, String estado) {
        this.cliente = cliente;
        this.valor = valor;
        this.estado = estado;
    }     
    
    public double VendasRJ(){        
        
        double imposto = (valor * 0.17);
        
                
        return imposto;
    }
    
    public double VendasSP(){
       
        double impostos = (valor * 0.18);
        
        return impostos;
    }
    
    public double VendasMA(){
        
        return valor * 0.12;
        
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    } 

}
