/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.VendaLivros;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class VendaLicrosTest {
    
    public VendaLicrosTest() {
    }
    @Test
    public void DeveCalcularImpostoPraRJ() {

        VendaLivros vendaRJ = new VendaLivros("Livraria RJ", 500, "RJ");
        assertEquals(85, vendaRJ.VendasRJ(), 0.01);
    }
    
    @Test
    public void DeveCalcularImpostoPraSP(){
        
        VendaLivros vendaSP = new VendaLivros("Livraria SP", 800, "SP" );
        assertEquals(144, vendaSP.VendasSP() , 0.01);     
    }
        
    @Test
    public void DeveCalcularImpostoPraMA(){
        VendaLivros vendaMA = new VendaLivros("Livraria de MA", 600 , "MA");
        assertEquals( 72 , vendaMA.VendasMA() , 0.01 );    
    }

}
